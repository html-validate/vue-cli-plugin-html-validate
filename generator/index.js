module.exports = (api) => {
	api.extendPackage({
		scripts: {
			"html-validate": "vue-cli-service html-validate",
		},
		devDependencies: {
			"html-validate": "^7",
			"html-validate-vue": "^5",
		},
	});

	api.render(`./template`);
};

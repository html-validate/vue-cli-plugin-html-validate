# vue-cli-plugin-html-validate changelog

## 4.1.0 (2024-12-29)

### Features

- **deps:** support html-validate v9 ([b301d84](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/b301d84c37ada42cbcf7861793a0f99c4dc89200))

## [4.0.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v3.0.0...v4.0.0) (2023-06-11)

### ⚠ BREAKING CHANGES

- **deps:** require html-validate v5 or later
- **deps:** require nodejs v16 or later

### Features

- **deps:** require html-validate v5 or later ([e17e3f6](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/e17e3f65ced2f3d08da4b63ee85e3a2221cc1e09))
- **deps:** require nodejs v16 or later ([d4bc04a](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/d4bc04aad7c3cd3a0b5a5ed841547acd26a63a48))

### Dependency upgrades

- **deps:** support html-validate v8 ([8a45162](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/8a45162ae6ca62afaa75fc765bdae5b8c8a39b4e))

## [3.0.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v2.0.4...v3.0.0) (2022-05-15)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([bfdee05](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/bfdee055d73ed849380f6a2b3780e63aa052d9f4))

### Dependency upgrades

- **deps:** support html-validate v7 ([cc435e6](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/cc435e69d27b4a525fc046d4be75cb9217f7a625))

### [2.0.4](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v2.0.3...v2.0.4) (2022-05-08)

### Bug Fixes

- install html-validate v6 ([7740def](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/7740defc15b0f28807a625edfc86d8ac62e4c9dc))

### [2.0.3](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v2.0.2...v2.0.3) (2022-02-27)

### Dependency upgrades

- **deps:** update dependency @vue/cli-shared-utils to v5 ([5f62c1b](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/5f62c1bcfa6784a6950a2f669b37e8ba679a6fb2))

### [2.0.2](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v2.0.1...v2.0.2) (2021-09-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([651d9d4](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/651d9d489b89fbc7b002e49a1dcf4c07a673ae79))

### [2.0.1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v2.0.0...v2.0.1) (2021-06-29)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([650f690](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/650f690fb278953d8e57ee6a9efeaafab33e72da))

## [2.0.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.5.0...v2.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([3f2903a](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/3f2903af14790bf5e4f27fad9c94f5d319fdcc28))

## [1.5.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.4.0...v1.5.0) (2020-12-20)

### Features

- support `--version` ([ee644d6](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/ee644d66735ee3020f67bed3281678f1049feacb))

## [1.4.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.3.0...v1.4.0) (2020-12-15)

### Features

- use `CLI` API to get validator instance ([1efa2bb](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/1efa2bb37bea4e9bebfc54889100517c2575c4e6))

## [1.3.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.5...v1.3.0) (2020-11-08)

### Features

- html-validate v4 compatibility ([fab3d49](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/fab3d497b7a8a6eeddbb472ff7f9de076f5cbd09))

## [1.2.5](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.4...v1.2.5) (2020-11-01)

## [1.2.4](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.3...v1.2.4) (2020-10-25)

### Bug Fixes

- use html-validate@3 ([4a2c09c](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/4a2c09cb7c85fcdfb951eb9392ffd18bb91349cc))

## [1.2.3](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.2...v1.2.3) (2020-05-28)

### Bug Fixes

- `property default is not expected to be here` ([1eac4ec](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/1eac4ecd4b79e7bc9a33aea27bdd3cfa9a7a182e)), closes [html-validate#91](https://gitlab.com/html-validate/issues/91)

## [1.2.2](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.1...v1.2.2) (2020-03-29)

### Bug Fixes

- update html-validate-vue ([3dd1cfc](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/3dd1cfca1b069abcadba6b28d509df8ae68637c0))

## [1.2.1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.2.0...v1.2.1) (2019-11-24)

### Bug Fixes

- fix typo in generated config ([5411fb1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/5411fb19f51554c389e67725fb1e64cd0716e0bb))

# [1.2.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.1.1...v1.2.0) (2019-11-24)

### Features

- update to html-validate 2 and html-validate-vue 2 ([d34069f](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/d34069f0a6999923e59ed594b506d6be60cb4fea))

## [1.1.1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.1.0...v1.1.1) (2019-11-18)

### Bug Fixes

- **deps:** update dependency @vue/cli-shared-utils to v4 ([3738361](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/3738361826a6507b568299ecbbeac95fa15c9fd7))

# [1.1.0](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/compare/v1.0.0...v1.1.0) (2019-09-28)

### Features

- adding generator ([f8dd880](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/f8dd880))
- generate .htmlvalidate.json ([96c020b](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/96c020b))
- support --config parameter ([42e07dd](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/42e07dd)), closes [#1](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/issues/1)

# 1.0.0 (2019-09-19)

### Features

- initial version ([73e0b2a](https://gitlab.com/html-validate/vue-cli-plugin-html-validate/commit/73e0b2a))

/* eslint-disable no-console -- expected to log to console */

/**
 * @typedef { import("html-validate").CLI } CLI
 */

const pkg = require("./package.json");

const DEFAULT_PATTERNS = ["src/**/*.vue", "src/**/*.html"];

/**
 * @returns {number}
 */
function getMaxWarnings(args) {
	return typeof args["max-warnings"] !== "undefined" ? args["max-warnings"] : -1;
}

/* eslint-disable-next-line complexity, sonarjs/cognitive-complexity -- techical debt */
module.exports = function lint(args, api) {
	const cwd = api.resolve(".");
	const { log, done, exit, loadModule } = require("@vue/cli-shared-utils");
	const { CLI, version } = loadModule("html-validate", cwd, true) || require("html-validate");

	if (args.version) {
		console.log(`html-validate-${version}`);
		console.log(`${pkg.name}-${pkg.version}`);
		return;
	}

	const patterns = args._ && args._.length ? args._ : DEFAULT_PATTERNS;

	/** @type {CLI} */
	const cli = new CLI({
		configFile: args.config ? `${cwd}/${args.config}` : undefined,
	});
	const htmlvalidate = cli.getValidator();
	const files = cli.expandFiles(patterns, { cwd });
	const formatter = cli.getFormatter(args.formatter || "codeframe");
	const report = htmlvalidate.validateMultipleFiles(files);

	const maxWarnings = getMaxWarnings(args);
	const isErrorsExceeded = report.errorCount > 0;
	const isWarningsExceeded = maxWarnings >= 0 && report.warningCount > maxWarnings;

	if (!isErrorsExceeded && !isWarningsExceeded) {
		if (!args.silent) {
			if (report.warningCount || report.errorCount) {
				console.log(formatter(report));
			} else {
				done(`No lint errors found!`);
			}
		}
	} else {
		console.log(formatter(report));
		if (isWarningsExceeded) {
			log(`html-validate found too many warnings (maximum: ${maxWarnings}).`);
		}
		exit(1);
	}
};
